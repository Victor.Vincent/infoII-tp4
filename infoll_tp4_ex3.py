class File:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        if item[0]>len(self.items):
            while len(self.items)<item[0]-1:
                self.items.append(())
            self.items.insert(len(self.items)-item[0], item)
        self.items[len(self.items)-item[0]]=item

    def dequeue(self):
        return self.items.pop()

    def is_empty(self):
        return (self.items == [])

    def size(self):
        return len(self.items)

file=File()
file.enqueue((4,"Nathan"))
print(file.items)
file.enqueue((2,"Julia"))
print(file.items)
file.enqueue((1,"Amandine"))
print(file.items)
file.enqueue((3,"Mathias"))
print(file.items)